plugins {
    alias(libs.plugins.configuration) apply false
    alias(libs.plugins.kotlin.serialization) apply false
    alias(libs.plugins.android.app) apply false
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.kotlin) apply false
    alias(libs.plugins.androidx.safeargs) apply false
    alias(libs.plugins.androidx.baselineprofile) apply false
}