package com.colorata.wallman.categories.api

import com.colorata.wallman.core.data.Strings


enum class WallpaperCategory(val locale: WallpaperCategoryLocale) {
    Appulse(Strings.appulse),
    Wonders(Strings.wonders),
    Peaceful(Strings.peaceful),
    Fancy(Strings.fancy),
    Garden(Strings.garden),
    Birdies(Strings.birdies),
    Minerals(Strings.minerals)
}